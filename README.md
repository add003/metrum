## Deutsche Lautschrift

### Vokale

#### Monophthonge

<img title="Vokaltrapez" alt="Vokaltrapez" src="./vokale.png" width="892">

| IPA-Zeichen |                           Beschreibung                            |                                           Beispiele                                            |
|:-----------:|:-----------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|     [a]     |                 ungerundeter offener Zentralvokal                 |              das [d***a***s] <br/> Country [ˈk***a***ntɹi] <br/> Cup [k***a***p]               |
|    [aː]     |             langer ungerundeter offener Zentralvokal              |                        haben [ˈh***aː***bn̩] <br/> Staat [ʃt***aː***t]                         |
|     [ɐ]     |                     fast offener Zentralvokal                     |                         über [ˈyːb***ɐ***] <br/> Lehrer [ˈleːʁ***ɐ***]                         |
|    [ɑ̃]     |            ungerundeter offener Hinterzungennasalvokal            |                                 Engagement [***ɑ̃***ɡaʒəˈmɑ̃ː]                                 |
|    [ɑ̃ː]    |        langer ungerundeter offener Hinterzungennasalvokal         |                  Engagement [ɑ̃ɡaʒəˈm***ɑ̃ː***] <br/> Nuance [nuˈ***ɑ̃ː***s]                   |
|     [e]     |         ungerundeter halbgeschlossener Vorderzungenvokal          |                  jedoch [j***e***ˈdɔx] <br/> Kaffee [ˈkaf***e***] ([kaˈfeː])                   |
|    [eː]     |      langer ungerundeter halbgeschlossener Vorderzungenvokal      |               der [d***eː***ɐ̯] <br/> Idee [iˈd***eː***] <br/> Mehl [m***eː***l]               |
|     [ɛ]     |            ungerundeter halboffener Vorderzungenvokal             |             es [***ɛ***s] <br/> März [m***ɛ***ʁt͡s] <br/> Camp [k***ɛ***mp] <br/>              |
|    [ɛː]     |         langer ungerundeter halboffener Vorderzungenvokal         |                  erklären [ɛɐ̯ˈkl***ɛː***ʁən] <br/> Dessert [dɛˈs***ɛː***ɐ̯]                   |
|    [ɛ̃ː]    |          ungerundeter halboffener Vorderzungennasalvokal          |        Pointe [ˈpo̯***ɛ̃ː***tə] <br/> Refrain [ʁəˈfʁ***ɛ̃ː***] <br/> Teint [t***ɛ̃ː***]        |
|     [ə]     |                      mittlerer Zentralvokal                       |     alle [ˈal***ə***] <br/> Aborigine [ɛb***ə***ˈɹɪd͡ʒini] <br/> Account [***ə***ˈkaʊ̯nt]      |
|     [i]     |           ungerundeter geschlossener Vorderzungenvokal            |      Minute [m***i***ˈnuːtə] <br/> vielleicht [f***i***ˈlaɪ̯çt] <br/> City [ˈsɪt***i***]       |
|    [iː]     |        langer ungerundeter geschlossener Vorderzungenvokal        |          die [d***iː***] <br/> ihr [***iː***ɐ̯] <br/> Teenager [ˈt***iː***nˌʔɛɪ̯d͡ʒɐ]          |
|     [ɪ]     | ungerundeter zentralisierter fast geschlossener Vorderzungenvokal |                  in [***ɪ***n] <br/> Insel [ˈɪnzl̩] <br/> Grinsen [ˈɡʁɪnzn̩]                   |
|     [o]     |          gerundeter halbgeschlossener Hinterzungenvokal           | Prozent [pʁ***o***ˈt͡sɛnt] <br/> Bungalow [ˈbʊŋɡal***o***] <br/> Restaurant [ʁɛst***o***ˈʁɑ̃ː] |
|    [oː]     |       langer gerundeter halbgeschlossener Hinterzungenvokal       |               Boot [b***oː***t] <br/> so [z***oː***] <br/> Niveau [niˈv***oː***]               |
|     [ɔ]     |             gerundeter halboffener Hinterzungenvokal              |                        von [f***ɔ***n] <br/> Chauffeur [ʃ***ɔ***ˈføːɐ̯]                        |
|     [ø]     |          gerundeter halbgeschlossener Vorderzungenvokal           |             ökologisch [***ø***koˈloːɡɪʃ] <br/> voyeuristisch [voj***ø***ˈʁɪstɪʃ]              |
|    [øː]     |       langer gerundeter halbgeschlossener Vorderzungenvokal       |    gehören [ɡəˈh***øː***ʁən] <br/> Goethe [ˈɡ***øː***tə] <br/> Regisseur [ʁeʒɪˈs***øː***ɐ̯]    |
|     [œ]     |             gerundeter halboffener Vorderzungenvokal              |                        können [ˈk***œ***nən] <br/> Pumps [p***œ***mps]                         |
|    [œː]     |          langer gerundeter halboffener Vorderzungenvokal          |                                    Burger [ˈb***œː***ɐ̯ɡɐ]                                     |
|    [œ̃ː]    |       langer gerundeter halboffener Vorderzungennasalvokal        |                              Parfum [paʁˈf***œ̃ː***] ([paʁˈfɛ̃ː])                              |
|     [u]     |            gerundeter geschlossener Hinterzungenvokal             |                     Januar [ˈjan***u***aːɐ̯] <br/> Tourist [t***u***ˈʁɪst]                     |
|    [uː]     |         langer gerundeter geschlossener Hinterzungenvokal         |               zu [t͡s***uː***] <br/> cool [k***uː***l] <br/> Tour [t***uː***ɐ̯]                |
|     [ʊ]     |  gerundeter zentralisierter fast geschlossener Hinterzungenvokal  |                        und [***ʊ***nt] <br/> Football [ˈf***ʊ***tˌbɔːl]                        |
|     [y]     |            gerundeter geschlossener Vorderzungenvokal             |                   Büro [b***y***ˈʁoː] <br/> analysieren [anal***y***ˈziːʁən]                   |
|    [yː]     |         langer gerundeter geschlossener Vorderzungenvokal         |                        für [f***yː***ɐ̯] <br/> typisch [ˈt***yː***pɪʃ]                         |
|     [ʏ]     |  gerundeter zentralisierter fast geschlossener Vorderzungenvokal  |                      müssen [ˈm***ʏ***sn̩] <br/> System [z***ʏ***sˈteːm]                       |

#### Diphthonge

| IPA-Zeichen |                                       Beispiele                                        |
|:-----------:|:--------------------------------------------------------------------------------------:|
|    [aɪ̯]    |           Mai [ma***ɪ̯***] <br/> sein [za***ɪ̯***n] <br/> Byte [ba***ɪ̯***t]           |
|    [aʊ̯]    |       auf [a***ʊ̯***f] <br/> Clown [kl***aʊ̯***n] <br/> County [ˈka***ʊ̯***nti]        |
|    [ɛɪ̯]    |          hey [h***ɛɪ̯***] <br/> Lady [ˈl***ɛɪ̯***di] <br/> Mail [m***ɛɪ̯***l]          |
|    [ɔɪ̯]    |     Gebäude [ɡəˈb***ɔɪ̯***də] <br/> neu [n***ɔɪ̯***] <br/> Joint [d͡ʒ***ɔɪ̯***nt]      |
|    [ɔʊ̯]    | Donut [ˈd***ɔʊ̯***nat] <br/> downloaden [ˈdaʊ̯nˌl***ɔʊ̯***dn̩] <br/> Show [ʃ***ɔʊ̯***] |
|    [ʊɪ̯]    |                                  pfui [p͡f***ʊɪ̯***]                                   |

### Konsonanten

|   IPA-Zeichen   |                 Beschreibung                 |                                             Beispiele                                             |
|:---------------:|:--------------------------------------------:|:-------------------------------------------------------------------------------------------------:|
| [ʔ] (Knacklaut) |         stimmloser glottaler Plosiv          |              abändern [ˈapˌ***ʔ***ɛndɐn] <br/> Theater [teˈ***ʔ***aːtɐ] ([teˈaːtɐ])               |
|       [b]       |        stimmhafter bilabialer Plosiv         |                          haben [ˈhaː***b***n̩] <br/> Hobby [ˈhɔ***b***i]                          |
| [ç] (ich-Laut)  |        stimmloser palataler Frikativ         |                           sich [zɪ***ç***] <br/> wenig [ˈveːnɪ***ç***]                            |
|       [d]       |        stimmhafter alveolarer Plosiv         |                           der [***d***eːɐ̯] <br/> Widder [ˈvɪ***d***ɐ]                            |
|       [f]       |      stimmloser labiodentaler Frikativ       |               für [***f***yːɐ̯] <br/> treffen [ˈtʁɛ***f***n̩] <br/> von [***f***ɔn]               |
|       [ɡ]       |          stimmhafter velarer Plosiv          |                      geben [ˈ***ɡ***eːbn̩] <br/> aggressiv [a***ɡ***ʁɛˈsiːf]                      |
|       [h]       |        stimmloser glottaler Frikativ         |                                       haben [ˈ***h***aːbn̩]                                       |
|       [j]       |       stimmhafer palataler Approximant       |                             Jahr [***j***aːɐ̯] <br/> Yen [***j***ɛn]                              |
|       [k]       |          stimmloser velarer Plosiv           |            können [ˈ***k***œnən] <br/> Quelle [ˈ***k***vɛlə] <br/> sechs [zɛ***k***s]             |
|       [l]       | stimmhafter lateraler alveolarer Approximant |                              alle [ˈa***l***ə] <br/> als [a***l***s]                              |
|       [m]       |         stimmhafter bilabialer Nasal         |                            kommen [ˈkɔ***m***ən] <br/> mit [***m***ɪt]                            |
|       [n]       |         stimmhafter alveolarer Nasal         |                          in [ɪ***n***] <br/> können [ˈkœ***n***ə***n***]                          |
|       [ŋ]       |          stimmhafter velarer Nasal           |                lang [la***ŋ***] <br/> hängen [ˈhɛ***ŋ***ən] <br/> jung [jʊ***ŋ***]                |
|       [p]       |         stimmloser bilabialer Plosiv         |                      Gruppe [ˈɡʁʊ***p***ə] <br/> Prozent [***p***ʁoˈt͡sɛnt]                       |
|       [ʁ]       |        stimmhafter uvularer Frikativ         |                                        durch [dʊ***ʁ***ç]                                         |
|       [ɹ]       |      stimmhafter alveolarer Approximant      |                                    Crew [k***ɹ***uː] ([kʁuː])                                     |
|       [s]       |        stimmloser alveolarer Frikativ        |                  das [da***s***] <br/> dass [da***s***] <br/> groß [ɡʁoː***s***]                  |
|       [ʃ]       |      stimmloser postalveolarer Frikativ      |          schon [***ʃ***oːn] <br/> spielen [ˈ***ʃ***piːlən] <br/> stehen [ˈ***ʃ***teːən]           |
|       [t]       |         stimmloser alveolarer Plosiv         |             Mitte [ˈmɪ***t***ə] <br/> Stadt [ʃ***t***a***t***] <br/> und [ʊn***t***]              |
|       [θ]       |         stimmloser dentaler Frikativ         |                                      Thriller [ˈ***θ***ɹɪlɐ]                                      |
|       [ð]       |        stimmhafter dentaler Frikativ         |                                     Smoothie [ˈsmuː***ð***i]                                      |
|       [v]       |      stimmhafter labiodentaler Frikativ      |       Quelle [ˈk***v***ɛlə] <br/> werden [ˈ***v***eːɐ̯dn̩] <br/> November [noˈ***v***ɛmbɐ]        |
|       [w]       |     stimmhafter labiovelarer Approximant     |                                     wow [***w***aʊ̯] ([vaʊ̯])                                     |
| [x] (ach-Laut)  |         stimmloser velarer Frikativ          |                 auch [aʊ̯***x***] <br/> Bucht [bʊ***x***t] <br/> Dach [da***x***]                 |
|       [z]       |       stimmhafter alveolarer Frikativ        |            Glaser [ˈɡlaː***z***ɐ] <br/> sechs [***z***ɛks] <br/> Wiese [ˈviː***z***ə]             |
|      [z̥]       |        stimmloser alveolarer Frikativ        | Absage [ˈapˌ***z̥***aːɡə] <br/> absolut [ap***z̥***oˈluːt] <br/> Großsegel [ˈɡʁoːsˌ***z̥***eːɡl̩] |
|       [ʒ]       |     stimmhafter postalveolarer Frikativ      |      Garage [ɡaˈʁaː***ʒ***ə] <br/> Jeanette [***ʒ***aˈnɛt] <br/> Plantage [planˈtaː***ʒ***ə]      |
|      [d͡ʒ]      |      stimmhafte postalveolare Affrikate      |        Dschungel [ˈ***d͡ʒ***ʊŋl̩] <br/> Job [***d͡ʒ***ɔp] <br/> Manager [ˈmɛnɪ***d͡ʒ***ɐ]         |
|      [p͡f]      |       stimmlose labiodentale Affrikate       |   Kopf [kɔ***p͡f***] <br/> pfropfen [ˈ***p͡f***ʁɔ***p͡f***n̩] <br/> sapphisch [ˈza***p͡f***ɪʃ]    |
|      [t͡s]      |        stimmlose alveolare Affrikate         |       bereits [bəˈʁaɪ̯***t͡s***] <br/> circa [ˈ***t͡s***ɪʁka] <br/> Skizze [ˈskɪ***t͡s***ə]       |
|      [t͡ʃ]      |      stimmlose postalveolare Affrikate       |            deutsch [dɔɪ̯***t͡ʃ***] <br/> Chip [***t͡ʃ***ɪp] <br/> Match [mɛ***t͡ʃ***]             |

### Sonstige Zeichen

|      IPA-Zeichen       |                                             Beschreibung                                              |
|:----------------------:|:-----------------------------------------------------------------------------------------------------:|
|    [<span>.</span>]    |                                             Silbengrenze                                              |
|    [<span>ˈ</span>]    |                       nachfolgende Silbe trägt primäre Betonung (Hauptbetonung)                       |
|    [<span>ˌ</span>]    |                      nachfolgende Silbe trägt sekundäre Betonung (Nebenbetonung)                      |
|    [<span>ː</span>]    |                        Längenzeichen; vorhergehender Laut wird lang gesprochen                        |
|    [<span>ˑ</span>]    |                      Längenzeichen; vorhergehender Laut wird halblang gesprochen                      |
|   [<span>◌̆</span>]    |                  Längenzeichen; gekennzeichneter Laut wird besonders kurz gesprochen                  |
|   [<span>◌̋</span>]    |                                          besonders hoher Ton                                          |
|   [<span>◌́</span>]    |                                               hoher Ton                                               |
|   [<span>◌̄</span>]    |                                             mittlerer Ton                                             |
|   [<span>◌̀</span>]    |                                             niedriger Ton                                             |
|   [<span>◌̏</span>]    |                                        besonders niedriger Ton                                        |
|   [<span>◌̌</span>]    |                                            steigender Ton                                             |
|   [<span>◌̂</span>]    |                                             fallender Ton                                             |
|   [<span>◌̤</span>]    |                                             Murmelstimme                                              |
|   [<span>◌̰</span>]    |                                              Knarrstimme                                              |
|   [<span>◌̥</span>]    |                            Stimmlose Aussprache des gekennzeichneten Lauts                            |
|   [<span>◌̬</span>]    |                           Stimmhafte Aussprache des gekennzeichneten Lauts                            |
|   [<span>◌̻</span>]    |                            Laminale Aussprache des gekennzeichneten Lauts                             |
|   [<span>◌̪</span>]    |                             Dentale Aussprache des gekennzeichneten Lauts                             |
|   [<span>◌̺</span>]    |                             Apikale Aussprache des gekennzeichneten Lauts                             |
|    [<span>ʰ</span>]    |                    Aspiriert gesprochen, also von einem hörbaren Lufthauch gefolgt                    |
|    [<span>ʲ</span>]    |                                       Palatalisiert gesprochen                                        |
|    [<span>ʷ</span>]    |                            Labialisiert gesprochen, also mit Lippenrundung                            |
|   [<span>◌̹</span>]    |                                Mit stärkerer Lippenrundung gesprochen                                 |
|   [<span>◌̜</span>]    |                               Mit schwächerer Lippenrundung gesprochen                                |
|   [<span>◌̟</span>]    |                                        weiter vorne gesprochen                                        |
|   [<span>◌̠</span>]    |                                       weiter hinten gesprochen                                        |
|   [<span>◌̈</span>]    |                                       zentralisiert gesprochen                                        |
|   [<span>◌̽</span>]    |                                  zur Mitte zentralisiert gesprochen                                   |
|   [<span>◌̩</span>]    |             Kennzeichnung eines Lauts, meist eines Konsonanten, der den Silbenkern bildet             |
|   [<span>◌̯</span>]    |            Kennzeichnung eines Lauts, meist eines Vokals, der nicht den Silbenkern bildet             |
|   [<span>◌̆</span>]    |                                          rhotisch gesprochen                                          |
|   [<span>◌̃</span>]    |                                           nasal gesprochen                                            |
|   [<span>◌̰</span>]    |               glottalisiert gesprochen, also mit Verengung oder Verschluss der Glottis                |
|   [<span>◌̝</span>]    |                                           angehobene Zunge                                            |
|   [<span>◌̞</span>]    |                                            gesenkte Zunge                                             |
|   [<span>◌̘</span>]    |                                      vorverlagerte Zungenwurzel                                       |
|   [<span>◌̙</span>]    |                                     zurückverlagerte Zungenwurzel                                     |
|   [<span>◌ᷠ</span>]    |                                            nasale Plosion                                             |
|   [<span>◌ᷝ</span>]    |                                           laterale Plosion                                            |
|   [<span>◌̚</span>]    |                                         keine hörbare Plosion                                         |
|   [<span>◌‿◌</span>]   |                      fließender Übergang zum nächsten Wort wie im Französischen                       |
| [<span>◌͡◌ ◌͜◌</span>] |           Ligaturbogen über oder unter zwei Lautzeichen bezeichnet eine Doppelartikulation            |
|    [<span>ꜜ</span>]    |                                               Downstep                                                |
|    [<span>ꜛ</span>]    |                                                Upstep                                                 |
|    [<span>↗</span>]    |                                         steigende Intonation                                          |
|    [<span>↘</span>]    |                                          fallende Intonation                                          |
|    [<span>ʼ</span>]    | Ejektive Aussprache; vorhergehendes Zeichen wird nicht pulmonal, sondern per Kehlkopfbewegung erzeugt |
| [<span>&nbsp;ˠ</span>] |                 velarisiert gesprochen, also mit Hebung der Hinterzunge an das Velum                  |
| [<span>&nbsp;ˤ</span>] |              pharyngalisiert gesprochen, also mit einer Engebildung im Rachen (Pharynx)               |
